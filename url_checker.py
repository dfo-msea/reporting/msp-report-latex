import requests
import csv
from urllib.error import HTTPError
import json

def check_open_maps_url(dataset_id, resource_id):
    """Check if a resource exists on open.canada.ca"""
    url = f"https://open.canada.ca/data/en/dataset/{dataset_id}/resource/{resource_id}"
    # First get the dataset info to verify it exists
    api_url = f"https://open.canada.ca/data/api/3/action/package_show?id={dataset_id}"
    try:
        response = requests.get(api_url)
        if response.status_code != 200:
            print(f"Dataset not found: {dataset_id}")
            return False
        data = response.json()
        resources = data['result']['resources']
        # Check if resource_id exists in the dataset
        resource_exists = any(r['id'] == resource_id for r in resources)
        if resource_exists:
            print(f"✓ Valid: {url}")
            return True
        else:
            print(f"✗ Resource not found: {resource_id} in dataset {dataset_id}")
            return False
    except Exception as e:
        print(f"Error checking {dataset_id}: {str(e)}")
        return False

# Dataset IDs from first list
dataset_ids = [
    "3f4e85af-cc34-4aab-8e25-b956a792739f",
    "661f58cc-e1c3-4249-ba62-08de5b464fe1",
    "51c60d88-c6ac-4e1c-9724-83b6048aeccd",
    "abbfe250-8202-4822-8bd1-aaaf393bbfd5",
    "2ccbf2d7-0b1c-4ee5-8d8f-43acc16ef1e1",
    "8a80c6f7-86a7-49e8-97c2-5229068e64cd",
    "1dcd28aa-99da-4f62-b157-15631379b170",
    "87a8ac43-c2c2-4159-bc35-b2eb928cac05",
    "75829a51-ef23-4e98-80c4-a6c24dc06858",
    "1c5b9302-cfd8-4386-8b2e-26973e3c3468",
    "fed5f00f-7b17-4ac2-95d6-f1a73858dac0",
    "e16b99f1-cc7d-4f2b-8661-6a1706ee7386",
    "cf612f99-e1e8-43fd-804e-65fe9c2814ee",
    "cec45ade-3647-4aec-84f1-8cb68dd305c2",
    "a67df54b-286d-4eb6-9b38-474f1efe86db",
    "ecca47d7-835b-419f-91ae-ae4f601070a3",
    "5551969d-f94c-406b-b849-50b49d32290f",
    "33974622-3acf-4f22-86c1-09b92fa91b2d",
    "d35d85c1-ae76-4f83-96f4-1b6d1c7dee60",
    "de96974b-9f19-499a-af27-7a47930c3ca2",
    "3dc54f1b-4c6c-4d54-8bb4-e5b56babfc4a",
    "1084a522-c2dd-47a0-81ac-d4810ce29056",
    "c2fa53cd-2a92-4412-b5cf-c111fd29e467",
    "19aefad9-c3e0-4486-9f5e-8e979f4825e0",
    "b431d6d7-e17a-4567-8fc9-ecad4b318958",
    "dc81e24d-bac3-4d6c-8f9d-ddfc63633f0d",
    "75139c26-e407-41fc-bc20-5b9ea871c009",
    "7b6176f0-671b-4d9d-aafe-041801c9ede9",
    "ceec7d97-59ff-422d-842c-d06b6626822e",
    "e9034436-5954-4a31-b33c-cd5328436162",
    "9fce9fac-80c5-4106-a09f-a69490adad27",
    "d958ff1e-9fdc-4aea-a459-b69e5a164578",
    "7fb93f81-48e2-404b-9ba5-c131a15ec782",
    "d069a70d-3eae-4663-98bd-20a3f6b674c6",
    "037661e7-11ec-4f1b-8e3d-1e756ac9e9f8",
    "88176a59-7c69-40c9-bcac-6b46e74bad99",
    "b26ea354-d50b-4581-9c84-67dd7abc1ac2",
    "9920e910-fce3-410c-a642-67b050186a64"
]

# Resource IDs from second list
resource_ids = [
    "44158919-50a4-4fec-abb0-a643938ba1e7",
    "38f7f491-6b94-4a06-b61e-0ebd0eba5897",
    "f6d10e8d-22fa-4ca4-92de-04abfd0b1a5f",
    "e802d430-d535-46fb-bff4-675dcde8f116",
    "be1901c6-33fc-47a1-83ae-3067ec9b7d8a",
    "dd9b5e8e-00c5-4a48-bd1c-07cdf7147b50",
    "c5c7e919-0594-43eb-9f9b-95c21bc8097d",
    "3185acfd-1c8c-4e39-b996-adffcb24c55e",
    "4c15fc5c-206d-4d91-bd3f-c9cf5b253b85",
    "56b77168-296d-491a-b546-10fc3946cc3e",
    "de1d4848-7f77-4c9d-a5fa-013769ca3b8c",
    "9ed56776-7ab0-402e-863e-57146da2a0f9",
    "1da4e3ab-1ecc-4535-b9bb-5bf9d06ef04e",
    "dd27f818-943c-4af3-a71f-d781128cd19f",
    "70ff2991-7eb3-4062-8d8f-602d36c42c22",
    "ff639678-ac8f-49a2-8d84-1eee01f9ec02",
    "b1b99e43-7b11-4723-a243-31f3f27b6a31",
    "59dab46c-798f-4e83-90bf-166ec0bbd069",
    "f0ce5c54-6d99-49f0-9591-c573633c11a4",
    "a0baa12b-77bd-49c5-b17a-1ea286f4284b",
    "9e4934f3-b4a7-40e3-a89c-85ba676b831e",
    "ec33ae7b-6ad6-4aad-8ccd-bdb262becd82",
    "f6e654d6-ef7e-4f8b-837b-73a7f946ce1f",
    "082ed7e1-0e57-4bd9-9d7c-4e6bced750ef",
    "933052ca-143d-4e7a-8132-6325a8dc38d8",
    "02000c96-a56f-423e-8f2b-3e72268a3ee6",
    "9194921f-e98a-4b2a-9ba7-42bfcc42dbca",
    "c789547a-ebc7-40e8-8df6-06b9c44b4364",
    "38d2c939-51c7-4693-89a7-172ee3cd1215",
    "feed1a3a-4562-40eb-97c7-fac86e2a8906",
    "79c1c669-c0a7-471e-93ab-7011dc57ffc5",
    "21cf8f3f-7318-4faf-b3a3-3b794517300d",
    "e2db5167-f7a8-4efa-bb7e-ba51fb1ec2cf",
    "d0c25ab1-9012-42f6-9e2d-fa39a8bd5b6d",
    "8b2757a0-a98c-4f58-9856-b489ddccb0c9",
    "c879dbb6-c64a-410e-b65d-05bde6c1d4c6",
    "63b188b5-0bfd-4b9c-a5b5-53b8ac213cb9",
    "984464cf-1b82-4542-b181-f1dcb1ea7c97"
]

# Check all combinations
for dataset_id, resource_id in zip(dataset_ids, resource_ids):
    print("\nChecking combination:")
    check_open_maps_url(dataset_id, resource_id)