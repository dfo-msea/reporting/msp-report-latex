% --- INTRODUCTION ---
\section{INTRODUCTION}
\subsection{Initiative de PSM}
Au Canada, les gouvernements fédéral, provinciaux et autochtones collaborent avec les intervenants pour faire progresser la 
planification spatiale marine (PSM) dans cinq zones de planification et mettre en place « un processus de planification 
et de gestion des océans transparent, inclusif et durable \citet{DFOa}. 
La PSM est un processus itératif de gestion durable de la répartition 
spatiale et temporelle des activités humaines dans les espaces océaniques afin d’atteindre des objectifs écologiques, économiques, 
culturels et sociaux (\citet{EhlerDouvere2009}). Elle fournit un cadre intégré qui prend en compte les interactions entre les différentes 
activités et entre les activités et le milieu marin. À ce titre, la PSM peut aider à soutenir une planification efficace des océans, 
à réduire les conflits et les effets anthropiques cumulatifs, à accroître la certitude quant aux investissements, à améliorer la 
coordination et les possibilités de mobilisation, et à contribuer aux possibilités du milieu marin 
(\citet{EhlerDouvere2009}, \citet{FrazaoSantos2019}). Les plans spatiaux marins élaborés au Canada visent à décrire la vision 
et les objectifs, à délimiter les frontières de la zone de planification, à donner une vue d’ensemble de son utilisation et des 
caractéristiques environnementales, et à mettre en évidence un engagement à travailler avec les partenaires de planification 
provinciaux, territoriaux et autochtones pour faire progresser leurs priorités1.

Dans la région du Pacifique, les efforts de PSM se sont concentrés sur la biorégion du plateau Nord (côte nord du Pacifique) et 
les biorégions du détroit de Georgia et du plateau Sud (sud de la Colombie-Britannique) (figure 1). La zone de planification pour 
la côte nord du Pacifique comprend le plateau continental depuis la frontière de l’Alaska, au sud, jusqu’à l’île Quadra et la péninsule 
Brooks, sur l’île de Vancouver. Depuis 2014, la planification d’un réseau d’aires marines protégées (AMP) est en cours sur la côte nord 
du Pacifique. Au début de l’année 2023, les gouvernements du Canada, de la Colombie-Britannique et de 15 Premières Nations ont 
publié un plan d’action pour ce réseau (\cite{MPANetworkReport2023}). Les efforts de planification spatiale marine à 
plus grande échelle s’appuieront sur les exercices de planification existants dans le Nord et sur l’achèvement de la planification en 
cours dans le sud de la Colombie-Britannique\footnote{\href{https://www.dfo-mpo.gc.ca/oceans/planning-planification/areas-aires/pacific-north-coast-cote-nord-pacifique-fra.html}{Zones de planification de l'espace marin (côte nord du Pacifique)}}. 
La zone d’étude de PSM dans le sud de la Colombie-Britannique commence à la frontière 
sud avec la côte nord du Pacifique (à la péninsule Brooks et à l’île Quadra) et s’étend vers le sud jusqu’à la frontière avec l’État de 
Washington. Cette région comprend la partie canadienne de la mer des Salish. Des efforts de PSM sont en cours dans le sud de la 
Colombie-Britannique en partenariat avec les ministères et organismes fédéraux, la province de la Colombie-Britannique et les 
gouvernements autochtones. Les premiers travaux se concentrent sur la coordination, la collecte d’information et la mobilisation des 
partenaires et des intervenants en PSM (\citet{MPASouthNetworkReport2024}).

\begin{figure}
    \centering
    \includegraphics[width=14.5cm]{reference/overview-map}
    \caption{Zones de planification spatiale marine (PSM) dans la région du Pacifique. La côte nord du Pacifique comprend la biorégion du plateau Nord et la PSM du sud de la Colombie-Britannique comprend les biorégions du détroit de Georgia et du plateau Sud.}
    \label{fig:overview-map}
\end{figure}\pagebreak

\subsection{Atlas de planification marine}
Les efforts de PSM sont intrinsèquement axés sur le lieu et dépendent des données spatiales pour les caractéristiques écologiques (espèces et habitats), 
les processus physiques et les activités humaines (\citet{EhlerDouvere2009}, \citet{FrazaoSantos2019}). L’Atlas de planification marine du Canada est un 
outil interactif conçu pour permettre aux partenaires, aux intervenants et aux autres utilisateurs d’accéder à des renseignements spatiaux à jour sur les 
caractéristiques et les activités pertinentes pour les différentes zones de planification. À ce jour, la version 1 des atlas a été élaborée pour les océans 
Atlantique\footnote{\href{https://egisp.dfo-mpo.gc.ca/apps/atlantic-atlas-atlantique/?locale=fr}{Atlas de planification marine du Canada - Atlantique}} et Pacifique\footnote{\href{https://egisp.dfo-mpo.gc.ca/apps/pacific-atlas-pacifique/?locale=fr}{Atlas de planification marine du Canada - Pacifique}}, et permet aux utilisateurs de visualiser et de comprendre les ensembles de données spatiales publiées. L’élaboration de l’atlas et 
des ensembles de données spatiales qu’il présente est en cours afin d’améliorer l’accès aux données océaniques, d’accroître la transparence, de faciliter 
la résolution des conflits et de cerner les lacunes en matière d’information\footnote{\href{https://www.dfo-mpo.gc.ca/oceans/planning-planification/atlas/about-au-sujet-eng.html}{À propos de l'atlas de la planification marine du Canada (l'atlas)}}.

\subsection{Élaboration de données spatiales par le Secteur des sciences du MPO}
Dans la région du Pacifique, les scientifiques de Pêches et Océans Canada (MPO) s’appuient sur des observations sur le terrain, des résultats 
tirés des modèles, des ensembles de données et de l’expertise du personnel de diverses disciplines pour élaborer des données spatiales et 
des méthodes d’analyse afin de répondre aux questions de recherche susceptibles d’éclairer l’élaboration de plans spatiaux marins. Les ensembles 
de données spatiales qui en résultent sont ensuite rendus disponibles par l’intermédiaire de la plateforme de données ouvertes du gouvernement du 
Canada  et des atlas de PSM. La publication d’ensembles de données spatiales sur des plateformes de données ouvertes est conforme à la Politique 
sur les données scientifiques du MPO (\citet{DFO2019}), car elle témoigne d’un engagement à gérer efficacement les données et à les rendre librement 
accessibles, ce qui favorise la transparence, la collaboration et l’intendance responsable des ressources marines.

Afin de veiller à ce que le travail pour l’initiative de PSM soit harmonisé avec le travail en cours pour d’autres initiatives dans la région du Pacifique, 
une série d’ateliers a été organisée pour le personnel du Secteur des sciences du Pacifique (\citet{Proudfoot2022}). Les participants ont pris 
connaissance de huit initiatives dans la région pour lesquelles des produits de données spatiales étaient en cours d’élaboration et des séances 
de travail propres à certains taxons ont été organisées pour discuter des projets en cours et des possibilités de collaboration et de partage de données. 

En s’appuyant sur la série d’ateliers sur les données spatiales, un inventaire des produits de données spatiales existants a été dressé afin 
d’aider à cerner les lacunes prioritaires en matière de données. Cet inventaire contient des ensembles de données spatiales élaborés pour 
atteindre un éventail d’objectifs différents dans l’ensemble des programmes du Secteur des sciences du MPO. Pour la côte nord du Pacifique, 
il comprend une vaste série d’ensembles de données spatiales qui ont été élaborés pour appuyer les analyses du processus de planification du 
réseau d’AMP et mis à disposition par l’entremise de la plateforme de cartographie en ligne SeaSketch\footnote{\href{https://www.seasketch.org/\#projecthomepage/59767c74bac2eb558ded3d9c/about}{Plateforme de cartographie en ligne SeaSketch}}. 
Bon nombre de ces ensembles de données 
ont été élaborés à l’échelle de la côte ou étendus à la zone de planification du sud de la Colombie-Britannique. 

Le personnel du Secteur des sciences du MPO a collaboré avec des dépositaires de données et des chercheurs afin de créer de nouveaux 
produits de données spatiales pour la PSM et d’appuyer la publication de données spatiales élaborées pour d’autres projets très pertinents en 
la matière. La priorité pour la création et la publication des données a été accordée aux ensembles de données écologiques qui ont permis de 
combler des lacunes de longue date sur différentes espèces et différents habitats et paramètres environnementaux, aux ensembles de données 
ayant une couverture spatiale complète pour la côte du Pacifique et aux produits de données actuels et à long terme. Les ensembles de données 
spatiales qui en ont résulté ont été rendus disponibles par l’intermédiaire de la plateforme de données ouvertes du gouvernement du Canada\footnote{\href{https://rechercher.ouvert.canada.ca/donneesouvertes/}{Portail du gouvernement ouvert}}. 

\subsection{Aperçu du rapport}
Le présent rapport technique contient un résumé des ensembles de données spatiales sur la PSM reposant sur les métadonnées disponibles 
dans leurs dossiers de données ouvertes. Un programme\footnote{\href{https://gitlab.com/dfo-msea/reporting/msp-report-latex}{Le programme Python est accessible à l’adresse suivante}} Python (\citet{Python2024}) 
a été élaboré pour extraire les métadonnées de chaque ensemble de 
données pertinent, celles-ci étant analysées en modèles et combinées à du texte statique. Les cartes sont générées par programme 
avant d’être intégrées au rapport. Étant donné que notre programme est conçu pour extraire des dossiers de métadonnées de façon dynamique, 
il a la capacité de mettre à jour le rapport avec des ensembles de données supplémentaires. 

Le rapport est organisé par caractéristiques écologiques (y compris les espèces et les habitats), ensembles de données océanographiques 
et activités humaines, notamment les empreintes des relevés de recherche. Chaque dossier comprend un aperçu de l’ensemble de données, 
un résumé des méthodes, des sources de données, et des éventuelles mises en garde ou incertitudes, ainsi que des 
cartes de chaque couche de données connexe. Pour certains ensembles de données comportant de nombreuses couches de données 
connexes (p. ex. climatologies océanographiques, modèles de poissons de fond), des exemples de cartes sont présentés. 
Les citations et les publications connexes sont incluses dans l'enregistrement OpenData pour chaque ensemble de données.

Le rapport comprend des ensembles de données compilés jusqu’en 2024. Les ensembles de données détaillés ci-dessous comprennent ceux 
qui ont été élaborés et publiés par le personnel du MPO pour la PSM en raison de leur pertinence pour la planification spatiale dans la région 
du Pacifique. D’autres ensembles de données élaborés ou publiés dans le cadre d’autres initiatives et qui ne sont pas détaillés dans ce 
rapport peuvent également être pertinents pour la PSM, en fonction des objectifs établis pour chaque zone de planification. Des liens 
vers certains de ces ensembles de données se trouvent dans l’Atlas de planification marine du Canada ou la plateforme de données ouvertes. 
Ne sont pas inclus dans ce rapport:

\begin{itemize}
    \item Les ensembles de données créés pour d’autres initiatives, notamment ceux publiés avec le soutien du personnel de PSM en 
    raison de leur pertinence pour la planification spatiale dans la région du Pacifique, ne sont pas inclus. Par exemple, le personnel de la 
    PSM a contribué à la publication d’une série d’ensembles de données délimitant les zones importantes\footnote{Exemple: \href{https://open.canada.ca/data/fr/dataset/fa8387ce-3c66-4d07-9406-2bd2fcb5cc35}{Zones importantes pour les cétacés dans la zone de gestion intégrée de la côte nord du Pacifique}} pour diverses espèces qui ont 
    été créés dans le cadre du processus d’identification des zones d’importance écologique et biologique (ZIEB) (\citet{ClarkeJamieson2006}). 
    \item Les ensembles de données non publiés et les ensembles de données publiés sur d’autres portails en ligne ne sont 
    pas non plus inclus dans ce rapport. Par exemple, l’ensemble de données complet sur la répartition de l’herbier de 
    zostère sur la côte du Pacifique\footnote{Park, A., Proudfoot, B., Robb, C.K. 2023. Eelgrass polygon data for the BC coast to 2023. Données non publiées.} est soumis à des contraintes de partage et n’a pas été publié ni inclus dans ce rapport, 
    mais des éléments partageables de l’ensemble de données ont été fournis au Groupe de travail national sur la zostère (NETForce) 
    et inclus dans l’ensemble de données nationales sur les zostères pour le Canada\footnote{\href{https://open.canada.ca/data/fr/dataset/a733fb88-ddaf-47f8-95bb-e107630e8e62}{Base de données nationale sur les zostères pour le Canada (NETForce)}}.
    \item Des ensembles de données spatiales compilés par d’autres organisations participant à l’initiative de PSM ne sont pas 
    présentés dans ce rapport. Par exemple, le rapport n’inclut pas de données spatiales détaillant les colonies d’oiseaux marins le 
    long de la côte du Pacifique qui ont été mises à jour à l’appui de la PSM par Environnement et Changement climatique Canada (Service canadien de la faune). 
  \end{itemize}
