% --- INTRODUCTION ---
\section{INTRODUCTION}
\subsection{MSP initiative}
In Canada, federal, provincial, and Indigenous governments are working with stakeholders to advance marine spatial planning (MSP) 
in five planning areas and develop a transparent, inclusive and sustainable oceans planning and management 
process (\citet{DFOa}). 
MSP is the iterative process of managing the spatial and temporal distribution of human activities in ocean spaces sustainably to 
meet ecological, economic, cultural, and social objectives (\citet{EhlerDouvere2009}). MSP provides an integrated framework 
that considers the interactions of different activities and between activities and the marine environment.  As such, MSP can 
help support efficient ocean planning, reduce conflicts and cumulative human impacts, increase investment certainty, 
improve coordination and opportunities for engagement, and contribute to marine conservation opportunities 
(\citet{EhlerDouvere2009}, \citet{FrazaoSantos2019}).  Marine spatial plans developed within Canada are intended to describe 
the vision and objectives, delineate the boundaries of the planning area, provide an overview of the use and environmental features, 
and reflect a commitment to working with provincial, territorial, and Indigenous planning partners to advance their priorities.

In the Pacific Region, MSP efforts have focused on the Northern Shelf bioregion (Pacific North Coast) and the Strait of Georgia and 
Southern Shelf bioregions (Southern British Columbia (BC)) (Figure 1).  The planning area for the Pacific North Coast includes the 
continental shelf from the Alaska border in the south to Quadra Island and Brooks Peninsula on Vancouver Island.  Since 2014, 
planning has been underway in Pacific North Coast for a network of marine protected areas (MPAs).  Early in 2023, a network 
action plan in Pacific North Coast was released by the governments of Canada, British Columbia, and 15 First Nations  (\citet{MPANetworkReport2023}).  Broader marine spatial planning efforts will build on existing planning exercises in the North while 
complementing planning underway in Southern BC\footnote{\href{https://www.dfo-mpo.gc.ca/oceans/planning-planification/areas-aires/pacific-north-coast-cote-nord-pacifique-eng.html}{Marine spatial planning areas (Pacific North Coast)}}.  
The study area for MSP in Southern BC begins at the southern border with Pacific North Coast (at Brooks Peninsula and Quadra Island) 
and extends south to the border with Washington state.  This region includes the Canadian portion of the Salish Sea.  MSP efforts in 
Southern BC are underway in partnership with federal departments and agencies, the Province of British Columbia and Indigenous 
governments with early work focused on coordination and information gathering and engaging with MSP partners and stakeholders (\citet{MPASouthNetworkReport2024}).

\begin{figure}
    \centering
    \includegraphics[width=14.5cm]{reference/overview-map}
    \caption{Planning areas for marine spatial planning (MSP) in the Pacific Region. Pacific North Coast includes the Northern Shelf Bioregion and Southern BC MSP includes the Strait of Georgia and Southern Shelf bioregions.}
    \label{fig:overview-map}
\end{figure}\pagebreak

\subsection{Marine Planning Atlas}
MSP efforts are inherently place-based and depend upon spatial data for ecological features (species and habitats), 
physical processes, and human activities (\citet{EhlerDouvere2009}, \citet{FrazaoSantos2019}).  
The Canada Marine Planning Atlas is an interactive tool designed to provide partners, stakeholders, 
and other users access to current spatial information on features and activities relevant to the different 
planning areas.  To date, version 1 atlases have been developed for the 
Atlantic\footnote{\href{https://egisp.dfo-mpo.gc.ca/apps/atlantic-atlas-atlantique/?locale=en}{Canadian Marine Atlas (Atlantic)}} 
and the Pacific\footnote{\href{https://egisp.dfo-mpo.gc.ca/apps/pacific-atlas-pacifique/?locale=en}{Canadian Marine Atlas (Pacific)}} 
oceans that allow users to view and understand published spatial datasets.  Development of the atlas, and 
the spatial datasets that it displays, is ongoing to improve access to ocean data, increase transparency, aid in 
conflict resolution, and identify information gaps\footnote{\href{https://www.dfo-mpo.gc.ca/oceans/planning-planification/atlas/about-au-sujet-eng.html}{About the Canada Marine Planning Atlas (the atlas)}}.

\subsection{Development of Spatial Data by DFO Science}
In the Pacific Region, scientists from Fisheries and Oceans Canada (DFO) are leveraging field observations, 
model outputs, datasets, and the expertise of staff across a variety of disciplines to develop spatial data 
and analysis methods to answer research questions that can inform the development of marine spatial plans. 
The resulting spatial datasets are then made available through the Government of Canada's Open Data platform\footnote{\href{https://open.canada.ca/en}{Open Government}} and the MSP atlases. Publishing spatial datasets 
to open data platforms aligns with DFO's Policy for Scientific Data (\citet{DFO2019}) by demonstrating a commitment 
to effective data management and making data openly accessible, which promotes transparency, collaboration, 
and the responsible stewardship of marine resources. 

To ensure work for the MSP initiative was aligned with work underway for other initiatives in the Pacific Region, 
a workshop series was held for Pacific Science staff (\citet{Proudfoot2022}).  Participants learned about eight 
Pacific initiatives for which spatial data products were being developed and taxa-specific working sessions were 
held to discuss current projects and opportunities for collaborative efforts and data sharing.  

Informed by the spatial data workshop series, an inventory of existing spatial data products was compiled to 
help identify priority data gaps.  The inventory contains spatial datasets developed to meet a variety of different 
objectives from across DFO Science programs.  For Pacific North Coast, this includes a large suite of spatial 
datasets that had been developed to support analyses for the MPA network planning process and made available 
through the SeaSketch online mapping platform\footnote{\href{https://www.seasketch.org/\#projecthomepage/59767c74bac2eb558ded3d9c/about}{The B.C. Northern Shelf Bioregion MPA Network SeaSketch Portal}}. 
Many of these datasets were developed at a coastwide scale or extended into the Southern BC planning area.    

DFO Science staff worked with data custodians and researchers to create new spatial data products for MSP 
and support the publication of spatial data developed for other projects that are highly relevant to MSP. 
Priority for data creation and publication was given to ecological datasets that helped to fill longstanding data 
gaps for different species, habitats, and environmental parameters, datasets with spatial coverage that was 
comprehensive for the Pacific coast, and current and long-term data products. The resulting spatial datasets 
were made available through the Government of Canada's Open Data platform\footnote{\href{https://search.open.canada.ca/opendata/}{Open Government Portal}}.  

\subsection{Overview of Report}
This technical report contains a summary of MSP spatial datasets using the metadata available through their Open Data records.  
A Python (\citet{Python2024}) program\footnote{\href{https://gitlab.com/dfo-msea/reporting/msp-report-latex}{Metadata Technical Report Code Repository}} 
was developed to retrieve metadata for each relevant dataset, which is parsed into templates and combined with static text. 
Maps are generated programmatically before being integrated into the report. Given that our program is designed to dynamically 
pull metadata records, it has the capability to update the report in the future with additional datasets.  

The report is organized by ecological features (including species and habitats), oceanographic datasets, and human activities, 
including research survey footprints. Each record includes an overview of the dataset, a summary of the methods, data sources, 
and any caveats or uncertainties, along with maps of each associated data layer.  For some datasets with many 
associated data layers (e.g., oceanographic climatologies, groundfish models), example maps are shown. 
Citations and related publications are included in the OpenData record for each dataset.

The report includes datasets compiled as of 2024.  Datasets detailed below include those developed and published by DFO staff for 
MSP because of their relevance for spatial planning in the Pacific Region. Other datasets that have been developed or published 
through other initiatives that are not detailed in this report may also be relevant for MSP purposes, depending on the objectives 
identified for each planning area. Links to some of these datasets may be found through the Canada Marine Planning Atlas or the 
Open Data platform.  Not included in this report are: 

\begin{itemize}
    \item Datasets created for other initiatives, including those published with support by MSP staff because of their relevance for spatial 
    planning in the Pacific Region, are not included.  For example, MSP staff helped publish a suite of datasets delineating Important Areas\footnote{Example: \href{https://open.canada.ca/data/en/dataset/fa8387ce-3c66-4d07-9406-2bd2fcb5cc35}{Important Areas for 
    Cetaceans in Pacific North Coast Integrated Management Area}} for a variety of species that were created through the process to 
    identify Ecologically and Biologically Significant Areas (EBSAs) (\citet{ClarkeJamieson2006}).   
    \item Unpublished datasets and datasets published through other online portals are also not included in this report. For example, 
    the full eelgrass bed distribution dataset for the Pacific coast\footnote{Park, A., Proudfoot, B., Robb, C.K. 2023. Eelgrass polygon data for the BC coast to 2023. Unpublished data.} 
    has sharing constraints and hasn't been published or included in this report but shareable components of the dataset have been provided to the National Eelgrass Taskforce (NETForce) 
    and included in the national eelgrass dataset for Canada\footnote{\href{https://open.canada.ca/data/en/dataset/a733fb88-ddaf-47f8-95bb-e107630e8e62}{National Eelgrass Dataset For Canada (NETForce)}}.
    \item Spatial datasets that have been compiled by other organizations involved in the MSP initiative are not displayed in this report.  
    For example, this report does not include spatial data detailing marine bird colonies along the Pacific Coast that have been updated 
    in support of MSP by Environment and Climate Change Canada (Canadian Wildlife Service).
  \end{itemize}
